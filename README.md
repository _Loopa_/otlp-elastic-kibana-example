# OTLP-Elastic-Kibana example

## Pipeline

1. __*OpenTelemetry SDK/Otel-cli*__ - collect data in ypur code and send metrics to OTLP Collector
2. __*OpenTelemetry Collector*__ - collect all observable data from agents and send to Elastic APM server
3. __*Elastic APM server*__ - collect all performances from OTLP (and other instances) and send to ElasticSearch
4. __*ElasticSearch & Kibana*__ - visualizations of data

Pipeline pic from [__Elastic official website__](https://www.elastic.co/guide/en/observability/current/open-telemetry.html)

![Pipeline picture](./pipeline.png)

## Start up demo

## Useful links

[OpenTelemetry API/SDK for python](https://opentelemetry.io/docs/languages/python/getting-started/) - good start for test scripts on Python and understand how it works 

[OpenTelemetry CLI](https://github.com/equinix-labs/otel-cli) - maybe helps to work with it, but not the best choice to start

[OpenTelemetry collector support for APM](https://www.elastic.co/guide/en/observability/current/open-telemetry-direct.html),
[Quick start for OTLP collector](https://opentelemetry.io/docs/collector/quick-start/),
[Collector installation](https://opentelemetry.io/docs/collector/installation/),
[Collector configuration](https://opentelemetry.io/docs/collector/configuration/) - to set up collector and link with APM server

[Elastic APM server](https://www.elastic.co/guide/en/observability/current/_apm_server_binary.html) - to set up APM server locally.
__More info__: [Docker installation of APM server](https://www.elastic.co/guide/en/observability/current/running-on-docker.html),
[Simple APM config](https://www.elastic.co/guide/en/observability/current/apm-server-configuration.html)

[Set up ElasticSearch & Kibana stack](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html) - to run stack