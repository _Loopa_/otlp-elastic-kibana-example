from src.otlp_tracer import TRACER, add_span, get_parent_trace_context, set_attr_to_current_span


@add_span(name="Image to Element Tree")
def test_function(a, b):
    return a + b


if __name__ == "__main__":
    print(test_function(1, 2))
