import os
from typing import Any

from opentelemetry import trace
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
)
from opentelemetry.exporter.otlp.proto.http.trace_exporter import OTLPSpanExporter as HTTPExporter
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter as GRPCExporter
from typing import Callable
from opentelemetry.trace import SpanContext, TraceFlags


# otlp default settings
ELASTIC_RESOURCE = Resource.create({
    "service.name": os.environ.get("OTEL_SERVICE_NAME", "test"),
    "service.version": os.environ.get("OTEL_SERVICE_VERSION", "1.0"),
    "deployment.environment": os.environ.get("OTEL_DEPLOYMENT_ENVIRONMENT", "production"),
})
# default endpoint "https://apm_server_url:8200"
ELASTIC_ENDPOINT = os.environ.get("OTEL_EXPORTER_OTLP_TRACES_ENDPOINT", "https://apm_server_url:8200")
OTLP_PROTOCOL = os.environ.get("OTEL_EXPORTER_OTLP_PROTOCOL", "http")


TRACE_PROVIDER = TracerProvider(resource=ELASTIC_RESOURCE)
if OTLP_PROTOCOL == "grpc":
    OTLP_EXPORTER = GRPCExporter(endpoint=ELASTIC_ENDPOINT)
else:
    OTLP_EXPORTER = HTTPExporter(endpoint=ELASTIC_ENDPOINT)
SPAN_PROCESSOR = BatchSpanProcessor(OTLP_EXPORTER)
TRACE_PROVIDER.add_span_processor(SPAN_PROCESSOR)
trace.set_tracer_provider(TRACE_PROVIDER)


if ELASTIC_ENDPOINT is not None:
    TRACER = trace.get_tracer("test")
else:
    TRACER = None


def get_parent_trace_context() -> SpanContext | None:
    """
    Get parent trace settings (set by envvars). If not set - return None (to create new trace)
    """
    trace_id = os.getenv('TRACE_ID') # 0xc62b5a1048b67857310acad3085748e3
    if trace_id is not None:
        trace_id = int(trace_id, 16)
    span_id = os.getenv('SPAN_ID')
    if span_id is not None:
        span_id = int(span_id, 16)
    if trace_id is not None or span_id is not None:
        return SpanContext(trace_id, span_id, True, trace_flags=TraceFlags(0x01))
    else:
        return None


def set_attr_to_current_span(attributes: dict[str, Any]) -> None:
    current_span = trace.get_current_span()
    for k, v in attributes.items():
        current_span.set_attribute(k, v)


def add_span(
        name: str | None = None,
        event_serializer: Callable = None,
        context: SpanContext | None = None,
        attributes: dict[str, Any] | None = None
):
    def decorator(func):
        def wrapper(*args, **kwargs):
            global TRACER
            if name is None:
                span_name = func.__name__
            else:
                span_name = name

            if TRACER is not None:
                with TRACER.start_as_current_span(span_name, context=context) as span:
                    res = func(*args, **kwargs)
                    if event_serializer is None:
                        span.add_event(str(res))
                    else:
                        span.add_event(event_serializer(res))

                    if attributes is not None:
                        for k, v in attributes.items():
                            span.set_attribute(k, v)
                    return res

            else:
                res = func(*args, **kwargs)
                return res
        return wrapper
    return decorator
